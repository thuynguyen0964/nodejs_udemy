const { Schema, model } = require('mongoose');
const validator = require('validator');

const schema = new Schema(
  {
    name: {
      type: String,
      required: true,
      minLength: [5, 'Name must be less than more 5 characters'],
    },
    email: {
      type: String,
      required: true,
      unique: true,
      lowercase: true,
      validate: [validator.isEmail, 'Please type valid email'],
    },
    password: {
      type: String,
      required: true,
      minLength: [5, 'Passwsord must be less than 5 characters'],
    },
    photo: String,
  },
  { timestamps: true }
);

const UserModel = model('User', schema);
module.exports = UserModel;
