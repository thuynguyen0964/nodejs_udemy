const tourModel = require('./tours');
const UserModel = require('./users');

module.exports = { tourModel, UserModel };
