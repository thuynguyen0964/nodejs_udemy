/* eslint-disable no-undef */
const express = require('express');
const defineConfig = require('./config');
const defineRoutes = require('./routes');
const connectDb = require('./db');
const dotenv = require('dotenv');
const { handleCatException, handleUnRejection } = require('./errors');

dotenv.config();
handleCatException();

const app = express();
const PORT = process.env.PORT;

const SERVER = app.listen(PORT, () =>
  console.log('Yah, sever are running....')
);

defineConfig(app);
defineRoutes(app);
connectDb();
handleUnRejection(SERVER);
