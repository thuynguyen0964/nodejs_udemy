const cors = require('cors');
const cookieParser = require('cookie-parser');
const express = require('express');

const defineConfig = (app) => {
  app.use(cors({ origin: true }));
  app.use(express.json());
  app.use(cookieParser());

  app.use(express.static('public'));
};
module.exports = defineConfig;
