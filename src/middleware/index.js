const myMiddleware = (req, res, next) => {
  req.requestTime = new Date().toLocaleString();
  next();
};

const idMiddleware = (req, res, next, value) => {
  req.params.id = value;
  console.log('Hello from id middleware 🖐🏻🖐🏻');
  next();
};

module.exports = { myMiddleware, idMiddleware };
