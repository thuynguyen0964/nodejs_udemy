/* eslint-disable no-unused-vars */
const {
  getAll,
  createNewTour,
  getOne,
  updateTours,
  deleteTour,
  getToursVerTwo,
} = require('../../services/natrous');
const { logger } = require('../../utils/helpers');
const { handlerError } = require('../error');

class ToursControllers {
  async aliasTopTours(req, res, next) {
    try {
      const { limit = 5, sort = 'price' } = req.query;
      next();
    } catch (error) {
      logger(error);
    }
  }

  async getTours(req, res, next) {
    const includesField = { ...req.query };

    try {
      const data = await getAll(includesField);
      res.status(data.status).json({ time: req.requestTime, ...data });
    } catch (err) {
      next(err);
    }
  }

  async getDetail(req, res) {
    try {
      const { id } = req.params;
      const data = await getOne(id);
      res.status(data?.statusCode).json({ ...data });
    } catch (err) {
      handlerError(err, req, res);
    }
  }

  async postTour(req, res) {
    try {
      const data = await createNewTour({ ...req.body });
      res.status(data.statusCode).json({ ...data });
    } catch (err) {
      handlerError(err, req, res);
    }
  }

  async patchTour(req, res) {
    try {
      const { id } = req.params;
      const data = await updateTours(id, req.body);
      res.status(data.statusCode).json({ ...data });
    } catch (err) {
      handlerError(err, req, res);
    }
  }

  async destroyTour(req, res) {
    const { id } = req.params;
    try {
      const data = await deleteTour(id);
      res.status(data.status).json({ ...data });
    } catch (err) {
      logger(err);
      res.status(500).json(err);
    }
  }

  async getTourStart(req, res) {
    try {
      const data = await getToursVerTwo(req.query);
      res.status(data.status).json({ ...data });
    } catch (err) {
      logger(err);
      res.status(404).json(err);
    }
  }
}

module.exports = new ToursControllers();
