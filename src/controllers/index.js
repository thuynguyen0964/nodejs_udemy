const ToursCtrl = require('./natrous/tours');
const authCtrl = require('./auth');

module.exports = { ToursCtrl, authCtrl };
