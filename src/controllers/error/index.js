/* eslint-disable no-undef */
const AppErr = require('../../utils/appErr');
const dotenv = require('dotenv');
const { cloneDeep, getValueErr } = require('../../utils/helpers');

dotenv.config();

const handleCastErr = (err) => {
  const message = `Invalid ${err.path} : ${err.value}`;
  return new AppErr(message, 400);
};

const handleDuplicate = (err) => {
  const value = getValueErr(err.message);
  const message = `Duplicate field value : ${value}`;
  return new AppErr(message, err.statusCode);
};

const handleValidator = (err) => {
  const errArr = Object.values(err.errors).map((el) => el?.message);
  const message = `Invalid input data ${errArr.join('. ')}`;
  return new AppErr(message, 400);
};

const sendErrProd = (err, res) => {
  if (err.isOperator) {
    res.status(err.statusCode).json({
      status: err.statusCode,
      message: err.message,
    });
  } else {
    res.status(500).json({
      status: 'error',
      message: 'Something went wrong, try again',
    });
  }
};

const handlerError = (err, req, res) => {
  err.statusCode = err.statusCode || 500;
  err.status = err.status || 'error';

  let error = cloneDeep(err);

  if (error.name === 'CastError') error = handleCastErr(error);
  if (error.code === 11000)
    error = handleDuplicate({ ...error, message: err.message });
  if (error.name === 'ValidationError') error = handleValidator(error);
  sendErrProd(error, res);
};

module.exports = { handlerError };
