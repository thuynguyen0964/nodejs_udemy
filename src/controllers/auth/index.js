const { handlerError } = require('../error');
const { register } = require('../../services/auth');

class authController {
  async registerUser(req, res) {
    try {
      const response = await register(req.body);
      res.status(response.statusCode).json({ ...response });
    } catch (err) {
      handlerError(err, req, res);
    }
  }

  async loginUser() {}
}

module.exports = new authController();
