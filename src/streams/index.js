const fs = require('fs');
const path = 'src/docs/README.md';

// solution 1
const solutionOne = () => {
  fs.readFile(path, (err) => {
    if (err) return null;
    console.log('Read file success');
  });
};

// solution 2 -> using streams
function solutionTwo(req, res) {
  const readable = fs.createReadStream(path);
  readable.on('data', (chunk) => {
    res.write(chunk);
  });

  readable.on('close', () => {
    res.end();
  });

  readable.on('error', (err) => {
    console.log(err.message);
    res.status(500);
    res.end('File not found');
  });
}

module.exports = { solutionOne, solutionTwo };
