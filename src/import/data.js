const fs = require('fs');
const { tourModel } = require('../models');
const { logger } = require('../utils/helpers');

const importData = async () => {
  try {
    const tours = JSON.parse(
      fs.readFileSync('src/resources/data/tours-simple.json', 'utf-8')
    );
    await tourModel.create(tours);
  } catch (err) {
    logger(err);
  }
};

module.exports = importData;
