const { UserModel } = require('../../models');
const { generateURL } = require('../../utils/helpers');

const register = async (body) => {
  const photoPath = generateURL(body.name);
  await UserModel.create({ ...body, photo: photoPath });
  return {
    status: 'success',
    statusCode: 201,
    message: 'Register successfully',
  };
};

const login = async () => {};

module.exports = { register, login };
