/* eslint-disable no-debugger */
const { tourModel } = require('../../models');
const LIMIT = 5;

const getAll = async (query) => {
  // Avanced querystring
  // let queryStr = JSON.stringify(query);
  // queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, (match) => `$${match}`);

  let { page = 1, limit = LIMIT, sort = 'createdAt', search = '' } = query;
  page = parseInt(page);
  limit = parseInt(limit) && limit >= LIMIT ? LIMIT : limit;

  // sorting
  const data = await tourModel
    .find({ name: { $regex: `.*${search}.*`, $options: 'i' } })
    .skip((page - 1) * limit)
    .limit(limit)
    .sort(sort);

  const count = await tourModel.countDocuments();

  return {
    message: 'Get all tours',
    status: 200,
    tours: data,
    results: data.length,
    page,
    totalPages: Math.ceil(count / limit),
    totalTours: count,
  };
};

const getOne = async (id) => {
  const tour = await tourModel.findById(id);
  if (tour)
    return {
      message: 'Get tour details ok!!',
      statusCode: 200,
      tour,
    };
  return { message: 'Tour not found', statusCode: 404 };
};

const createNewTour = async (body) => {
  await tourModel.create({ ...body });
  return { message: 'Create tours ok!!', statusCode: 201 };
};

const updateTours = async (id, body) => {
  await tourModel.findByIdAndUpdate(id, body, { runValidators: true });
  return { message: 'Update tour success', statusCode: 200 };
};

const deleteTour = async (id) => {
  await tourModel.findByIdAndDelete(id);
  return { message: 'Delete tour ok!', status: 204 };
};

const getToursVerTwo = async (query) => {
  let { search = '', sort = 'createdAt', limit = LIMIT, page = 1 } = query;
  page = parseInt(page);
  limit = parseInt(limit);
  limit >= LIMIT ? LIMIT : limit;

  const offset = (page - 1) * limit;

  const data = await tourModel
    .aggregate([
      {
        $match: { name: { $regex: `.*${search}.*`, $options: 'i' } },
      },
      /* {
        $group: {
          _id: 'difficulty',
          totalTours: { $sum: 1 },
          avgPrice: { $avg: '$price' },
        },
      }, */
      { $skip: offset },
      { $limit: limit },
    ])
    .sort(sort);

  return {
    message: 'Get all tours ok!!',
    status: 200,
    size: data.length,
    page,
    tours: data,
  };
};

module.exports = {
  getAll,
  createNewTour,
  getOne,
  updateTours,
  deleteTour,
  getToursVerTwo,
};
