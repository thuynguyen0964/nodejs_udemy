/* eslint-disable no-undef */
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

const DB_URI = process.env.MONGO_URI.replace(
  '<PASSWORD>',
  process.env.MONGO_PASSWORD
);

const connectDb = async () => {
  await mongoose.connect(DB_URI);
  console.log('Connect mongoose ok!!!');
};

module.exports = connectDb;
