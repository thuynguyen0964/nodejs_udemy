function logger(params = null) {
  if (params) {
    console.log(params.toString());
  }
}

const randomId = (num) => Math.floor(Math.random() * num);

const cloneDeep = (params) => {
  return JSON.parse(JSON.stringify(params));
};

const findUserByEmail = async (schema, email) => {
  const user = await schema.findOne({ email });
  if (user) {
    return user;
  }
  return false;
};

const generateURL = (name, options = {}) => {
  const { color = 'orange', bg = 'white' } = options;
  return `https://placehold.co/600x400/${color}/${bg}?text=${name}`;
};

const getValueErr = (message) => {
  const regex = /{[^:]+:\s*"([^"]+)"/;
  const match = regex.exec(message);
  if (match) return match[1];
  return null;
};

module.exports = {
  logger,
  randomId,
  cloneDeep,
  findUserByEmail,
  generateURL,
  getValueErr,
};
