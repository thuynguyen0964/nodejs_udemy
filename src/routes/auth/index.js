const { Router } = require('express');
const { authCtrl } = require('../../controllers');

const authRoute = Router();

authRoute.post('/register', authCtrl.registerUser);

authRoute.post('/login', authCtrl.loginUser);

module.exports = authRoute;
