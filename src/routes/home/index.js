const { Router } = require('express');

const homeRoutes = Router();

homeRoutes.get('/', (req, res) => {
  res.status(200).json({ message: 'Yah , first project with Jonas...' });
});

homeRoutes.post('/', (req, res) => {
  const data = req.body;
  res.status(201).json({ ...data });
});

module.exports = homeRoutes;
