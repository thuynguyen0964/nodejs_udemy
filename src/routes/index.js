const homeRoutes = require('./home');
const toursRoutes = require('./natrours/tours');
const AppError = require('../utils/appErr');
const authRoute = require('./auth');

const defineRoutes = (app) => {
  app.use('/', homeRoutes);

  app.use('/api/v1/tours', toursRoutes);

  app.use('/auth', authRoute);

  app.all('*', (req, res, next) => {
    next(new AppError(`Can not find ${req.url} on server`, 404));
  });
};
module.exports = defineRoutes;
