const toursRoutes = require('express').Router();
const { ToursCtrl } = require('../../controllers');
const { myMiddleware, idMiddleware } = require('../../middleware');

toursRoutes.param('id', idMiddleware);

toursRoutes.get('/', myMiddleware, ToursCtrl.getTours);

toursRoutes.get(
  '/top-5-cheap',
  myMiddleware,
  ToursCtrl.aliasTopTours,
  ToursCtrl.getTours
);

toursRoutes.get('/tour-stats', myMiddleware, ToursCtrl.getTourStart);

toursRoutes.post('/new', ToursCtrl.postTour);

toursRoutes.get('/:id', ToursCtrl.getDetail);

toursRoutes.patch('/:id', ToursCtrl.patchTour);

toursRoutes.delete('/:id', ToursCtrl.destroyTour);

module.exports = toursRoutes;
